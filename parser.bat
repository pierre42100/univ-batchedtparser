@ECHO OFF

REM
REM EDT Parser for Univ Lyon 1
REM
REM @author Pierre HUBERT 2018
REM

REM
REM Important note : If your encounter hour precision issue,
REM do not complain my software. It has not been designed to
REM completely parse dates (in fact I am not sure it could be
REM done in batch. Instead of that, please change the value of
REM the hour_adjustment variables...)
REM

REM File configuration
SET TempDownloadFileName=downloaded.fullbatchcal.tmp
SET TempFilesExt=".batchcal.tmp"
SET FilesSeparator==
SET hour_adjustment=0
SET res_id=41580

REM Reset variables
SET dtstart="none"
SET dtend="none"
SET summary="none"
SET location="none"

REM Download events list

REM Determine date
SET request_date=%DATE:~6,4%-%DATE:~3,2%-%DATE:~0,2%
SET request_url="http://adelb.univ-lyon1.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources=%res_id%&projectId=2&calType=ical&firstDate=%request_date%&lastDate=%request_date%"

REM Download file (UNIX went here)
wget -O %TempDownloadFileName% %request_url%
REM Downloaded file (UNIX left the conversation)

REM Process the list of events
FOR /F "eol=- delims=: tokens=1,*" %%i IN (%TempDownloadFileName%) DO CALL :processevent %%i "%%j"

GOTO :after_process_event

REM Process the list of event
:processevent
REM Process date start
IF /I %1 EQU DTSTART (
	SET dtstart=%2
)

REM Process date end
IF /I %1 EQU DTEND (
	SET dtend=%2
)

REM Process summary
IF /I %1 EQU SUMMARY (
	SET summary=%2
)

REM Process location
IF /I %1 EQU LOCATION (
	SET location=%2
)

REM Save event
IF /I %1 EQU END (
	
	IF /I %2 EQU "VEVENT" (
		REM Save the even to a file
		ECHO %dtstart%%FilesSeparator%%dtend%%FilesSeparator%%summary%%FilesSeparator%%location% > %dtstart%%TempFilesExt%
	)
)
GOTO :EOF


:after_process_event

REM Now it is time to display the list of events
FOR %%v IN (*%TempFilesExt%) DO (
	
	REM Display each event
	FOR /F "eol=- delims== tokens=1,2,3,4" %%i IN (%%v) DO CALL :display_event %%i %%j %%k %%l
	
)


GOTO :after_display_event
:display_event

SET begin_date=%~1
SET end_date=%~2

SET begin_hour=%begin_date:~9,2%
SET /A begin_hour+=%hour_adjustment%
SET begin_minutes=%begin_date:~11,2%

IF %begin_hour%==%hour_adjustment% CALL :fixbeginhour

IF /I %begin_hour% LSS 10 (
	SET begin_hour=0%begin_hour%
)

SET end_hour=%end_date:~9,2%
SET /A end_hour+=%hour_adjustment%
SET end_minute=%end_date:~11,2%


IF %end_hour%==%hour_adjustment% CALL :fixendhour

IF /I %end_hour% LSS 10 (
	SET end_hour=0%end_hour%
)


ECHO %begin_hour%:%begin_minutes% %end_hour%:%end_minute% %~3 (%~4)

GOTO :EOF

:fixbeginhour
SET begin_hour=%begin_date:~10,1%
SET /A begin_hour+=%hour_adjustment%
GOTO :EOF

:fixendhour
SET end_hour=%end_date:~10,1%
SET /A end_hour+=%hour_adjustment%
GOTO :EOF

REM We finally did it !
:after_display_event

REM Do some cleanup
DEL %TempDownloadFileName%
DEL *%TempFilesExt%

ECHO End of script.